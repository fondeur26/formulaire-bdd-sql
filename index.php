<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulaire de contact</title>
    <style>
            .bouton {
                 height: 100%;
                width: 100%;
                padding-left: 15px;
                font-size: 18px;
                 outline: none;
                background: none;
                color: #070707;
                border: 2px solid #99f309;}
            
        </style>
    </head>
    <body>


<div class="formul">

<h1 style="color:#000000">Complétez ce formulaire</h1>



<form action="formulaire.php" method="post">
    
    <p><i>Complétez le formulaire. Les champs marqué par </i><em>*</em> sont <em>obligatoires</em></p>
    <fieldset style="color:#000000">
         <legend>Contact</legend><br>
        
        <label>Prénom <em>*</em></label><br><br>
        <input type="text" name="prenom" pattern="[A-Za-z-]{2,}" maxlength="50" required><br><br>
        
        <label>Nom <em>*</em></label><br><br>
        <input type="text" name="nom" pattern="[A-Za-z-]{2,}" maxlength="50" required><br><br>
        
        <label>Portable</label>
        <br><br>
        <input id="telephone" name="telephone" type="telephone" placeholder="06xxxxxxxx" pattern="[0]{1}[1-9]{1}[0-9]{8}"><br><br>
        
        <label>Sujet <em>*</em></label><br><br>
        <input id="sujet" name="sujet" type="sujet" placeholder="sujet" required pattern="[A-Za-z]{2,}" maxlenght="200"><br>
        <br>

        <label>Email <em>*</em></label><br><br>
        <input id="email" name="email" type="email" placeholder="Email" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"><br>
        <br>
        
        <label for="message">Laissez nous un message : <em>*</em></label><br><br>
        <textarea id="message" name="message" rows="5" cols="33" required pattern="[A-Za-z]{2,}" maxlenght="1000"></textarea>
    </fieldset>

    <p><input class="bouton" type="submit" value="Envoyer"></p>
</form>

</div>
<a href='code.php'>Traitement des données(info normalement mot de passe)</a>
</body>
</html>