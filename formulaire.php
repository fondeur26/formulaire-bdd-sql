<?php
//1 Les données obligatoires sont elles présentes ?
if (!empty($_POST)) { 

if( isset( $_POST["prenom"]) && isset( $_POST["nom"]) && isset( $_POST["email"]) && isset( $_POST["message"]) && isset( $_POST["telephone"])){
 
    //2 Les données obligatoires sont-elles remplies ?
    if( !empty( $_POST["prenom"]) && !empty( $_POST["nom"]) && !empty( $_POST["email"]) && !empty( $_POST["message"])){
 
        //3 Les données obligatoires ont-elles une valeur conforme  la forme attendue ?
        if( filter_var($_POST["email"], FILTER_SANITIZE_EMAIL)){
 
            $_POST["email"] = filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);
 
            if( filter_var($_POST["message"], FILTER_SANITIZE_FULL_SPECIAL_CHARS)){
                $_POST["message"] = filter_var($_POST["message"], FILTER_SANITIZE_FULL_SPECIAL_CHARS);
 
                if( filter_var($_POST["prenom"], FILTER_SANITIZE_STRING) && filter_var($_POST["nom"], FILTER_SANITIZE_STRING) && filter_var($_POST["telephone"], FILTER_SANITIZE_STRING)) {
                    $_POST["prenom"] = filter_var($_POST["prenom"], FILTER_SANITIZE_STRING);
                    $_POST["nom"] = filter_var($_POST["nom"], FILTER_SANITIZE_STRING);
                    $_POST["telephone"] = filter_var($_POST["telephone"], FILTER_SANITIZE_STRING);
                    
                   
 
                    //5 affiche le message
                    printf('Bonjour, votre message a bien été envoyé.<br><a href="index.php">Retour à la page d\'accueil</a>');
                    
                    //Envoit du mail dans la BDD

                    //les variables

                    $nom = $_POST['nom'];
                    $prenom = $_POST['prenom'];
                    $telephone = $_POST['telephone'];
                    $email = $_POST['email'];
                    $sujet = $_POST['sujet'];
                    $message = $_POST['message'];

                    //Connection à la BDD

                    try {
                        require("pdo.php");

                        //Fonction pour se connecter t_email et récup un email                        
                        //Envoyer les données en BDD

                        if (! ($MyDB->inTransaction())) {
                            try {
                        
                                    $MyDB->beginTransaction();

                                    //D'abord l'email

                                    //on test si l'email est présent en BDD

                                    $emailBDD = $MyDB->prepare("SELECT id_Email, Email FROM t_email WHERE Email = :valEmail");
                                    $emailBDD->bindParam(':valEmail', $email);
                                    $emailBDD->execute();
                                    $infos = $emailBDD->fetchAll(PDO::FETCH_OBJ);

                                    //Si le tableau est vide insère dans la table t_email : l'email

                                    if(empty($infos)){

                                        //On insère l'email

                                        require_once("requette.php");
                                        
                                        $requete = $MyDB->prepare(REQ_INSERT_MAIL);
                                        $requete->bindParam(':valEmail', $email);
                                        $requete->execute();

                                        //Ensuite récup id_email

                                        $recup_IdEmail = $MyDB->prepare(RECUP_ID_EMAIL);
                                        $recup_IdEmail->bindParam(':valueEmail', $email);
                                        $recup_IdEmail->execute();
                                        $datas = $recup_IdEmail->fetchAll(PDO::FETCH_OBJ);

                                        foreach($datas as $mail):

                                            $idMail = $mail->id_Email;

                                            //On envoit les données dans t_personne 

                                            require_once("requette.php");

                                            $insertInfo = $MyDB->prepare(REQ_INSERT_PERSONNE);
                                            $insertInfo->bindParam(':prenom', $prenom);
                                            $insertInfo->bindParam(':nom', $nom);
                                            $insertInfo->bindParam(':tel', $telephone);
                                            $insertInfo->bindParam(':id_email', $idMail);
                                            $insertInfo->execute();

                                            //On insère les datas dans t_msg

                                            $insertmsg = $MyDB->prepare(REQ_INSERT_MSG);
                                            $insertmsg->bindParam(':sujet', $sujet);
                                            $insertmsg->bindParam(':msg', $message);
                                            $insertmsg->bindParam(':id_email', $idMail);
                                            $insertmsg->execute();

                                        endforeach;   

                                    }
                                    else{
                                        foreach($infos as $mail):
                                        
                                        
                                        
                                        //Si l'email est déjà présent
                                        //Si le tableau est plein, on récupère l'id de l'email

                                        if($mail->Email === $email){

                                            $Id_mail = $mail->id_Email;
                                            echo "le mail existe";

                                            require_once("requette.php");

                                            //On insère les datas dans t_personne

                                            $insertInfo = $MyDB->prepare(REQ_INSERT_PERSON);
                                            $insertInfo->bindParam(':prenom', $prenom);
                                            $insertInfo->bindParam(':nom', $nom);
                                            $insertInfo->bindParam(':tel', $telephone);
                                            $insertInfo->bindParam(':id_email', $Id_mail);
                                            $insertInfo->execute();

                                            //On insère les datas dans t_msg
                                            
                                            $insertMsg = $MyDB->prepare(REQ_INSERT_MESSAGE);
                                            $insertMsg->bindParam(':sujet', $sujet);
                                            $insertMsg->bindParam(':msg', $message);
                                            $insertMsg->bindParam(':id_email', $Id_mail);
                                            $insertMsg->execute();  
                                            
                                        }                                   
                                        endforeach;

                                        
                                    }    

                                    


                                    $MyDB->commit();
                        
                            } catch (Exception $ExceptionRaised) {
                                    $MyDB->rollBack();
                                    print_r($MyDB->errorInfo());
                                    print_r($MyRequest->errorInfo());
                                    die("Error: " . $ExceptionRaised->getMessage());
                            }
                        }else{
                         print 'Impossible de commencer la transaction. Une autre transaction est déjà en cours.';
                        }

                    } catch (Exception $ExceptionRaised) {
                        printf("Erreur dans l'envoit du message. Impossible de traîter les données");

                    }
                    
                
                    
                }else {print("Il semble que les données que vous avez saisies ne soient pas valides");
                }
 
            }else {print("Votre message contient sans doute des caractéres non appropriés");
            }
            
          }else {print("Merci de saisir une adresse email valide");
          }

        }else {print("Votre message contient sans doute des caractéres non appropriés");
        }
 
    }else {print("Merci de remplir les champs obligatoires");
    }
 
}else {print("ok");
}
 

