<?php
declare(strict_types = 1);

//Demande des fichiers avec require_once

require_once "DbServerParams.php";


function RequestManager(object $RequestToHandle = null, string $InfoMessage = "", string $NoDataMessage = "", array $RequestParams = [], $IsSelectRequest = true)
{
    if (is_a($RequestToHandle, "PDOStatement")) {
        try {
            (boolean) $ReqSuccess = $RequestToHandle->execute($RequestParams);
            if ($ReqSuccess) {
                print PHP_EOL . $InfoMessage . PHP_EOL . PHP_EOL;
                if ($IsSelectRequest) {
                    while ($DataLine = $RequestToHandle->fetch(PDO::FETCH_ASSOC)) {
                        print_r($DataLine);
                    }
                }
            } else {
                print $NoDataMessage . PHP_EOL . PHP_EOL;
            }
        } catch (Exception $ExceptionRaised) {
            print "Une erreur inattendue est survenue !" . PHP_EOL;
            print ("Erreur : " . $ExceptionRaised->getMessage()) . PHP_EOL;
            print "Informations sur l'état de la requète :" . PHP_EOL;
            print_r($RequestToHandle->errorInfo());
        }
    } else {
        print "ERREUR !!! La variable passée en 1er argument de RequestManager() n'est pas une requète de type PDOStatement" . PHP_EOL . PHP_EOL;
    }
}

(object) $MyDB = null;
(boolean) $PromptPwd = false;

//Connection BDD

$MyDB = new PDO(DB_DRIVER . ":host=" . DB_HOST . ";dbname=" . DB_NAME, DEFAULT_DB_LOGIN, DEFAULT_DB_PWD);