<?php

//  Les requêtes pour le formulaire.php (récupérer et analyser les données) //
//Insérer les données dans les trois tables

//Commencer par l'email si l'email n'existe pas

const REQ_INSERT_MAIL = "INSERT INTO t_email (Email) values (:valEmail)";

const RECUP_ID_EMAIL = "SELECT id_Email FROM t_email WHERE Email = :valueEmail";

const REQ_INSERT_PERSONNE = "INSERT INTO t_personne (prenom, nom, tel, id_email_personne) VALUES (:prenom, :nom, :tel, :id_email)";

const REQ_INSERT_MSG = "INSERT INTO t_msg(sujet, msg, id_email) VALUES (:sujet, :msg, :id_email)";

//requêtes à faire si l'email existe bien bdd

const REQ_INSERT_PERSON = "INSERT INTO t_personne (prenom, nom, tel, id_email_personne) values (:prenom, :nom, :tel, :id_email)";

const REQ_INSERT_MESSAGE = "INSERT INTO t_msg (sujet, msg, id_email) values (:sujet, :msg, :id_email)";


//Fin




//Afficher tous les messages avec une fonction getMessage()

const REQ_MESSAGES_DATE = "SELECT id_msg, sujet, msg, date_msg, nom, prenom, tel, etat, Email  FROM t_msg join t_personne join t_email where t_personne.id_email_personne = t_email.id_Email and t_msg.id_Email = t_email.id_Email ORDER BY date_msg desc";

function getMessage (){

try {

    require_once("pdo.php");

    $messageInfo = $MyDB->prepare(REQ_MESSAGES_DATE);
    $messageInfo->execute();
    if($messageInfo->rowCount() > 0) 
    {
        return $messageInfo;
    }
    else{
        return "";
    }
    
$messageInfo->closeCursor();
    
} catch (Exception $e) {
    echo 'Erreur reçue : ',  $e->getMessage(), "\n";
}



}

//Afficher la liste des messages en attente de traitement ("À traiter" et "À relancer")avec une fonction Message()

const REQ_MESSAGES_URG = "SELECT id_msg, sujet, msg, date_msg, nom, prenom, tel, etat, Email FROM t_msg join t_personne join t_email ON t_personne.id_email_personne = t_email.id_Email and t_msg.id_Email = t_email.id_Email WHERE etat = 'A traiter' OR etat = 'A relancer'ORDER BY date_msg desc";

function getMessageUrg (){

try {

    require_once("pdo.php");

    $messageIn = $MyDB->prepare(REQ_MESSAGES_URG);
    $messageIn->execute();
    if($messageIn->rowCount() > 0) //si il y a des données
    {
        return $messageIn;
    }
    else{
        return "";
    }
    
$messageIn->closeCursor();
    
} catch (Exception $e) {
    echo 'Erreur reçue : ',  $e->getMessageUrg(), "\n";
}



}


//Transaction pour modifier l' état d'un message marquer "À traiter" (valeur par défaut), "À relancer", "En attente de réponse", "RDV pris" et "Sans suite".
//Utilisation de la fonction changeEtat;

const REQ_MODIF_ETAT = "UPDATE t_msg SET etat = :ValEtat WHERE id_msg = :id";

function changeEtat ($id, $etat){

    try {

        require_once("pdo.php");

        if (! ($MyDB->inTransaction())) {
            try {
        
                $MyDB->beginTransaction();

                    $insertEtat = $MyDB->prepare(REQ_MODIF_ETAT);
                    $insertEtat->bindParam(':ValEtat', $etat);
                    $insertEtat->bindParam(':id', $id);
                    $insertEtat->execute();

                $MyDB->commit();
            }
            catch (Exception $ExceptionRaised) {

                $MyDB->rollBack();
                print_r($MyDB->errorInfo());
                print_r($MyRequest->errorInfo());
                die("Error: " . $ExceptionRaised->getMessage());
            }

        }
    }
    catch (Exception $ExceptionRaised) {
        
        printf("Erreur, impossible de maudifier l'état");

    }

    
}

const REQ_VERIF_EMAIL = "SELECT id_msg, sujet, msg, date_msg, nom, prenom, tel, etat, Email FROM t_msg join t_personne join t_email ON t_personne.id_email_personne = t_email.id_Email and t_msg.id_Email = t_email.id_Email WHERE Email = :valEmail ORDER BY date_msg desc;";

function getMessageMail($email){
 
    try {

       require_once("pdo.php");

       if(empty($email)){
           header("Location: code.php");
       }
       if(filter_var($email, FILTER_SANITIZE_EMAIL)){ 
           
           $email = filter_var($email, FILTER_SANITIZE_EMAIL);

           $messagePerso = $MyDB->prepare(REQ_VERIF_EMAIL);
           $messagePerso->bindParam(':valEmail', $email);
           $messagePerso->execute();

           if($messagePerso->rowCount() > 0) //si il y a des données
           {
               return $messagePerso;
           }
           else{
               header("Location: code.php");
           }    
           $messagePerso->closeCursor();
       }
       else{
           header("Location: code.php");
       }

   } catch (Exception $e) {
   echo 'Erreur reçue : ',  $e->getMessageMail(), "\n";
   }   

}

//SELECT id_msg, sujet, msg, date_msg, nom, prenom, tel, etat, Email FROM t_msg join t_personne join t_email ON t_personne.id_email_personne = t_email.id_Email and t_msg.id_Email = t_email.id_Email WHERE Email = 'fond.rv@gmail.com' ORDER BY date_msg desc;
