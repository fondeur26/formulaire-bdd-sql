<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel='stylesheet' href="css/index.css"/>

    <title>Traitement des informations </title>
</head>

<div class='etat'>
<h1>Page de traitement des messages du formulaire de contact</h1>


    <h3>Afficher tous les messages</h3>

    <form action="code.php" method='post'>
        <label for='affiche'>Afficher tous les messages</label>
        <input type='submit' value='affiche' name='affiche' id='affiche' />
    </form>

<?php

    //On affiche tous les messages dans un tableau

if(isset($_POST['affiche']) && !(empty($_POST['affiche']))):

?>
    <div class='affiche'>
        

        <table>
            <tr>
                <th>ID</th>
                <th>Sujet</th>
                <th>message</th>
                <th>Date</th>
                <th>Nom</th>
                <th>Prénom</th>
                <th>Téléphone</th>
                <th>état msg</th>
                <th>Email</th>
            </tr>
            

<?php  
            require_once("requette.php");

            $messages = getMessage();

            while ($msgs = $messages->fetch(PDO::FETCH_NUM)){

            //var_dump($msgs);   
            
            printf("<tr>");

                foreach($msgs as $ligne){
                    
                    printf('<td>'.$ligne.'</td>');
                    
                }

            printf("</tr>");

            }
            

            ?>

        </table>

    </div>
    
    <form action='code.php' method='post' class='btn'>
        <input type="submit" value='Cacher le tableau' class='btn'/>
    </form>

    <?php

    
else:
    echo '';
endif;

//Fin de l'affichage des messages dans un tableau


?>
    <h3>Afficher la liste des messages en attente de traitement "À traiter" et "À relancer"</h3>

<form action="code.php" method='post'>
    <label for='affich'>Afficher tous les messages</label>
    <input type='submit' value='affiche' name='affich' id='affich' />
</form>

<?php

//On affiche tous les messages dans un tableau

if(isset($_POST['affich']) && !(empty($_POST['affich']))):

?>
<div class='affich'>
    

    <table>
        <tr>
            <th>ID</th>
            <th>Sujet</th>
            <th>message</th>
            <th>Date</th>
            <th>Nom</th>
            <th>Prénom</th>
            <th>Téléphone</th>
            <th>état msg</th>
            <th>Email</th>
        </tr>
        

<?php  
        require_once("requette.php");

        $messag = getMessageUrg();

        while ($msg = $messag->fetch(PDO::FETCH_NUM)){

        //var_dump($msgs);   
        
        printf("<tr>");

            foreach($msg as $ligne){
                
                printf('<td>'.$ligne.'</td>');
                
            }

        printf("</tr>");

        }
        

        ?>

    </table>

</div>

<form action='code.php' method='post' class='btn'>
    <input type="submit" value='Cacher le tableau' class='btn'/>
</form>

<?php


else:
echo '';
endif;

//Fin de l'affichage des messages dans un tableau




?>
<h3>Modifier le statut d'un messages</h3>




<form action="code.php" method='post'>

        <p><label for='affiche'>Rentrer l'id du messages : </label></p>

        <input type='text' pattern="[0-9]{1,}" maxlength="3" name='id' id='id' required/>
        <div>
            <input type="radio" id="suite" name="etat" value='Sans suite'
                checked>
            <label for="suite">Sans suite</label>
        </div>

        <div>
            <input type="radio" id="relancer" name="etat" value='A relancer'>
            <label for="relancer">A relancer</label>
        </div>

        <div>
            <input type="radio" id="traiter" name="etat" value='A traiter'>
            <label for="traiter">A traiter</label>
        </div>

        <div>
            <input type="radio" id="attente" name="etat" value='En attente'>
            <label for="attente">En attente</label>
        </div>
        <div>
            <input type="radio" id="RDV Pris" name="etat" value='RDV Pris'>
            <label for="RDV Pris">RDV Pris</label>
        </div>

        <input type='submit' value='Modifier' name='modifier' id='modif' />

        
</form>

<?php


if(!(empty($_POST['etat'])) && !(empty($_POST['id']))):

    require_once("requette.php");

    

    $etat = htmlspecialchars($_POST['etat']);
    $id = htmlspecialchars($_POST['id']);

    $ID = intval($id);

    if($ID){

        changeEtat($ID, $etat);

        printf("L'état a bien été modifié");
    }
    else{
        printf('Erreur dans le traitement');
    }

else: 
    printf("");

endif;
?>
<h3>recherche par Email</h3>

<form action="code.php" method='post'>

        
<label>Email <em>*</em></label><br><br>
        <input id="email" name="email" type="email" placeholder="Email" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"><br>
        <br>
        <input type='submit' value='affiche' name='affic' id='affic' />
      
</form>

<?php
//  var_dump($_POST['email']);
 
if(isset($_POST['email']) && !(empty($_POST['email']))):
    
    require_once("requette.php");
 
    
 
    $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
 
    //var_dump($mail);
   
    
    if($email){
 
        $result = getMessageMail($email);
        // var_dump($result);
    ?>

    <table class="affic">
            <tr>
            <th>ID</th>
            <th>Sujet</th>
            <th>message</th>
            <th>Date</th>
            <th>Nom</th>
            <th>Prénom</th>
            <th>Téléphone</th>
            <th>état msg</th>
            <th>Email</th>
            </tr>
<?php
 
        while ($data = $result->fetch(PDO::FETCH_NUM)){

     
    
                printf("<tr>");

                foreach($data as $element){
            
                printf('<td>'.$element.'</td>');
            
        }
// var_dump($data);
    printf("</tr>");

    }
 
?>
 
    </table>
 
     <form action='code.php' method='post' class='btn bg-danger'>
            <input type="submit" value='Cacher le tableau' class='btn'/>
     </form>
 
   


 
    <?php            
    }
    else{
        printf("Erreur dans le traitement");
    }
 
else: 
    printf("");
 
endif;
?>

</div>

</body>

</html>